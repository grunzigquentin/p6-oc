package com.projet6.oc.controllers;

import com.projet6.oc.model.entity.EntityArticles;
import com.projet6.oc.model.entity.EntityComments;
import com.projet6.oc.model.request.CreateArticle;
import com.projet6.oc.model.request.CreateComment;
import com.projet6.oc.services.ArticlesServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController()
@RequestMapping("/articles")
public class ArticlesController {
    @Autowired
    ArticlesServices articlesServices;
    @GetMapping()
    public List<EntityArticles> GetArticlesByThemes(Principal principal){
        return this.articlesServices.getArticlesByUser(principal.getName());
    }

    @PostMapping()
    public EntityArticles createArticles(@RequestBody CreateArticle createArticle){
        return this.articlesServices.createArticles(createArticle);
    }

    @GetMapping("/{id}")
    public EntityArticles GetArticleById(@PathVariable("id") Integer id){
        return this.articlesServices.getArticleById(id);
    }

    @PostMapping("/{id}/comment")
    public EntityComments createComment(@PathVariable("id") Integer id, @RequestBody CreateComment createComment, Principal principal) {
        return this.articlesServices.postComment(id, createComment, principal.getName());
    }
}
