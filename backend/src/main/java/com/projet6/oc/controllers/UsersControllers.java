package com.projet6.oc.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.projet6.oc.model.entity.EntityUsers;
import com.projet6.oc.model.mappers.UsersMapper;
import com.projet6.oc.model.request.CreateUserRequest;
import com.projet6.oc.model.request.Logging;
import com.projet6.oc.model.request.UpdateUserRequest;
import com.projet6.oc.model.response.UserDTO;
import com.projet6.oc.services.UsersServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;

@RestController()
@RequestMapping("/users")
public class UsersControllers {
    @Autowired

    UsersServices usersServices;

    @PostMapping("/register")
    public String createUser(@RequestBody CreateUserRequest createUserRequest) throws JsonProcessingException {
        usersServices.validateUser(createUserRequest);
        EntityUsers entityUsers = UsersMapper.INSTANCE.userRequestToEntityUser(createUserRequest);

        EntityUsers createUsers = usersServices.createUser(entityUsers);
        Logging logging = new Logging(createUserRequest.getEmail(), createUserRequest.getPassword());
        return usersServices.userAuthentication(logging);
    }


    @PostMapping("/login")
    public String token(@RequestBody Logging logging) throws JsonProcessingException {
        return usersServices.userAuthentication(logging);
    }

    @GetMapping("/me")
    public UserDTO getUser(Principal principal){
        EntityUsers user = usersServices.getUser(principal.getName());
        return UsersMapper.INSTANCE.userEntityToUserDTO(user);
    }

    @PutMapping("/profile")
    public UserDTO updateUser(Principal principal, @RequestBody UpdateUserRequest updateUserRequest){
        EntityUsers user = usersServices.getUser(principal.getName());
        return UsersMapper.INSTANCE.userEntityToUserDTO(usersServices.updateUser(user,updateUserRequest));
    }

}
