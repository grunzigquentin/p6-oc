package com.projet6.oc.controllers;

import com.projet6.oc.model.entity.EntityThemes;
import com.projet6.oc.model.entity.EntityUsers;
import com.projet6.oc.model.request.SubscribeRequest;
import com.projet6.oc.services.ThemesServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
@RestController()
@RequestMapping("/themes")
public class ThemesControllers {
    @Autowired
    ThemesServices themesServices;
    @GetMapping()
    public List<EntityThemes> getAllThemes(){
    return themesServices.getAllThemes();
    }

    @PostMapping("/subscribe")
    public EntityUsers subscribeToThemes(Principal principal, @RequestBody SubscribeRequest subscribeRequest){
        return themesServices.subscribeToTheme(subscribeRequest.themeId(), principal.getName());
    }

    @GetMapping("/forUser")
    public List<EntityThemes> getThemesByUser(Principal principal){
        return themesServices.getThemeByUser(principal.getName());
    }
}
