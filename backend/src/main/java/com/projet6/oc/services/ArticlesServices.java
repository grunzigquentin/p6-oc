package com.projet6.oc.services;

import com.projet6.oc.model.entity.EntityArticles;
import com.projet6.oc.model.entity.EntityComments;
import com.projet6.oc.model.entity.EntityThemes;
import com.projet6.oc.model.entity.EntityUsers;
import com.projet6.oc.model.repository.EntityArticlesRepository;
import com.projet6.oc.model.repository.EntityCommentRepository;
import com.projet6.oc.model.repository.EntityThemesRepository;
import com.projet6.oc.model.repository.EntityUsersRepository;
import com.projet6.oc.model.request.CreateArticle;
import com.projet6.oc.model.request.CreateComment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service

public class ArticlesServices {
    @Autowired
    EntityArticlesRepository entityArticlesRepository;

    @Autowired
    EntityThemesRepository entityThemesRepository;

    @Autowired
    EntityUsersRepository entityUsersRepository;

    @Autowired
    EntityCommentRepository entityCommentsRepository;

    public EntityArticles createArticles(CreateArticle createArticle){
        EntityThemes entityThemes = entityThemesRepository.getThemesById(createArticle.themes());
        EntityArticles entityArticles = new EntityArticles();
        entityArticles.setContent(createArticle.content());
        entityArticles.setTitle(createArticle.title());
        entityArticles.setThemes(entityThemes);
        return this.entityArticlesRepository.save(entityArticles);
    }
    /**
     * This will get the current user who send the request.
     * It will then take the themes they have.
     * Finally it put those articles in a List of articles send back to the user.
     * */
    public List<EntityArticles> getArticlesByUser(String email){
        EntityUsers entityUsers = this.entityUsersRepository.findByEmail(email);
        Set<EntityThemes> setEntityThemes = entityUsers.getThemes();

        List<EntityArticles> articles = new ArrayList<>();
        for (EntityThemes theme : setEntityThemes) {
            articles.addAll(theme.getArticles());
        }

        return articles;
    }

    public EntityArticles getArticleById(Integer id){
        return this.entityArticlesRepository.getArticlesById(id);
    }

    /**
     With the same logic of the function getArticle by user
     it will get the data of the user sending the request, and
     the data of the article to be modified. Then JPA will do
     all the required modification under the hood.
     * */
    public EntityComments postComment(Integer article, CreateComment createComment, String email){
        EntityUsers entityUsers = this.entityUsersRepository.findByEmail(email);

        EntityArticles entityArticles = this.entityArticlesRepository.getArticlesById(article);
        EntityComments entityComments = new EntityComments();
        entityComments.setComment(createComment.comment());
        entityComments.setArticles(entityArticles);
        entityComments.setEntityUsers(entityUsers);

        return this.entityCommentsRepository.save(entityComments);
    }
}
