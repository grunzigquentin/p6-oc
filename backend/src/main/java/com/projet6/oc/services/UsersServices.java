package com.projet6.oc.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.projet6.oc.model.entity.EntityUsers;
import com.projet6.oc.model.repository.EntityUsersRepository;
import com.projet6.oc.model.request.CreateUserRequest;
import com.projet6.oc.model.request.Logging;
import com.projet6.oc.model.request.UpdateUserRequest;
import com.projet6.oc.model.response.TokenResponse;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Set;

@Service
public class UsersServices {
    @Autowired
    EntityUsersRepository entityUsersRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtServices jwtServices;

    public UsersServices(EntityUsersRepository entityUsersRepository, AuthenticationManager authenticationManager){
        this.entityUsersRepository = entityUsersRepository;
    }
    public EntityUsers createUser(EntityUsers entityUsers) {
        entityUsers.setPassword(bCryptPasswordEncoder.encode(entityUsers.getPassword()));
        try {
          return entityUsersRepository.save(entityUsers);

        }
        catch (DataIntegrityViolationException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Email already used");
        }
    }

    public EntityUsers updateUser(EntityUsers entityUsers, UpdateUserRequest updateUserRequest){
        entityUsers.setEmail(updateUserRequest.email());
        entityUsers.setUsername(updateUserRequest.username());

        return entityUsersRepository.save(entityUsers);
    }
    public EntityUsers getUser(String email){
        return entityUsersRepository.findByEmail(email);
    }

    public void validateUser(CreateUserRequest createUserRequest){
        Validator validator;
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        Set<ConstraintViolation<CreateUserRequest>> violations = validator.validate(createUserRequest);

        if(violations.isEmpty()){
            return;
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, violations.iterator().next().getMessage());
    }
    /**
     * @param logging is the object containing the email and password used for authentication
     * @return TokenResponse in a json format
     */
    public String userAuthentication(Logging logging) throws JsonProcessingException {
        UsernamePasswordAuthenticationToken log = UsernamePasswordAuthenticationToken.unauthenticated(logging.email(), logging.password());
        Authentication authentication = authenticationManager.authenticate(log);
        TokenResponse tokenResponse = new TokenResponse(jwtServices.generateToken(authentication));

        return tokenResponse.toString();
    }


}
