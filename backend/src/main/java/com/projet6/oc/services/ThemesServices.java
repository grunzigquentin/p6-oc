package com.projet6.oc.services;

import com.projet6.oc.model.entity.EntityThemes;
import com.projet6.oc.model.entity.EntityUsers;
import com.projet6.oc.model.repository.EntityThemesRepository;
import com.projet6.oc.model.repository.EntityUsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ThemesServices {


    @Autowired
    EntityThemesRepository entityThemesRepository;

    @Autowired
    EntityUsersRepository entityUsersRepository;

    public List<EntityThemes> getAllThemes(){
        return this.entityThemesRepository.findAll();
    }

    public EntityUsers subscribeToTheme(Integer theme, String emailUser){
        EntityThemes entityThemes = this.entityThemesRepository.getThemesById(theme);
        EntityUsers entityUsers = this.entityUsersRepository.findByEmail(emailUser);

        entityThemes.getUsers().add(entityUsers);
        entityUsers.getThemes().add(entityThemes);
        return this.entityUsersRepository.save(entityUsers);
    }

    public List<EntityThemes> getThemeByUser(String emailUser){
        EntityUsers entityUsers = this.entityUsersRepository.findByEmail(emailUser);
        return this.entityThemesRepository.findByUsers(entityUsers);
    }
}
