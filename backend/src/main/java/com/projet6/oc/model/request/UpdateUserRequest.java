package com.projet6.oc.model.request;

public record UpdateUserRequest(String email, String username) {
}
