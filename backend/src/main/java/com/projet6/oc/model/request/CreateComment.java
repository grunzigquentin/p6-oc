package com.projet6.oc.model.request;

public record CreateComment (String comment) {
}