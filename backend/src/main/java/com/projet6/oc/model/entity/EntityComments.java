package com.projet6.oc.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "COMMENTS")
public class EntityComments {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String comment;

    @OneToOne
    @JoinColumn(name="user_id")
    @JsonManagedReference
    private EntityUsers entityUsers;


    @ManyToOne
    @JoinColumn(name="article_id")
    @JsonBackReference
    private EntityArticles articles;
}
