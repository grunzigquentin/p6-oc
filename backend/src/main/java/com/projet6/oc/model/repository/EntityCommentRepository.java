package com.projet6.oc.model.repository;

import com.projet6.oc.model.entity.EntityComments;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntityCommentRepository  extends JpaRepository<EntityComments, Integer> {
}
