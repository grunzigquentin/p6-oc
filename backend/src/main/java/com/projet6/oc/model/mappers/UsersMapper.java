package com.projet6.oc.model.mappers;

import com.projet6.oc.model.entity.EntityUsers;
import com.projet6.oc.model.request.CreateUserRequest;
import com.projet6.oc.model.response.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UsersMapper {
    UsersMapper INSTANCE = Mappers.getMapper(UsersMapper.class);
    EntityUsers userRequestToEntityUser(CreateUserRequest createUserRequest);

    UserDTO userEntityToUserDTO(EntityUsers entityUsers);
}
