package com.projet6.oc.model.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "THEMES")
public class EntityThemes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String title;

    @ManyToMany(mappedBy = "themes")
    @JsonBackReference
    private Set<EntityUsers> users;

    @OneToMany(mappedBy="themes")
    @JsonBackReference
    private Set<EntityArticles> articles;
}
