package com.projet6.oc.model.response;

import com.projet6.oc.model.entity.EntityThemes;
import java.util.Set;

public record UserDTO(String username, String email, Set<EntityThemes> themes)
{
}
