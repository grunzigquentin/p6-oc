package com.projet6.oc.model.request;

public record SubscribeRequest(Integer themeId) {
}
