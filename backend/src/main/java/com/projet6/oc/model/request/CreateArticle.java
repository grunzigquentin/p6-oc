package com.projet6.oc.model.request;

public record CreateArticle(String content, String title, Integer themes) {
}

