package com.projet6.oc.model.repository;

import com.projet6.oc.model.entity.EntityArticles;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EntityArticlesRepository extends JpaRepository<EntityArticles, Integer> {
    public EntityArticles getArticlesById(Integer id);
}
