package com.projet6.oc.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "ARTICLES")
public class EntityArticles {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String content;

    private String title;

    @ManyToOne
    @JoinColumn(name="theme_id")
    @JsonManagedReference
    private EntityThemes themes;

    @OneToMany(mappedBy="articles")
    @JsonManagedReference
    private Set<EntityComments> comments;
}
