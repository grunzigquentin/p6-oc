package com.projet6.oc.model.repository;

import com.projet6.oc.model.entity.EntityArticles;
import com.projet6.oc.model.entity.EntityThemes;
import com.projet6.oc.model.entity.EntityUsers;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EntityThemesRepository extends JpaRepository<EntityThemes, Integer> {

    public EntityThemes getThemesById(Integer id);

    List<EntityThemes> findByUsers(EntityUsers entityUsers);


}