package com.projet6.oc.model.request;

import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateUserRequest {
    @Email(message = "Email is of incorrect value")
    @NotNull(message = "Email cannot be null")
    @Size(max = 50,  message = "Email cannot be more than 50 characters" )
    private String email;

    @NotNull(message = "Password cannot be null")
    @Size(max = 50, message = "Password cannot be more than 50 characters")
    @Size(min = 8, message = "Password must be more than 8 characters")
    @Pattern(regexp = "(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=.{8,})\\S+$", message = "Password must at least have one special, lowercase, uppercase, and one number character")
    private String password;

    @NotNull(message = "Username cannot be null")
    @Size(min = 1, message = "username cannot be empty")
    @Size(max = 50, message = "username cannot be more than 50 characters")
    private String username;
}
