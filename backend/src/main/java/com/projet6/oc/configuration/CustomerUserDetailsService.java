package com.projet6.oc.configuration;

import com.projet6.oc.model.entity.EntityUsers;
import com.projet6.oc.model.repository.EntityUsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Service

public class CustomerUserDetailsService implements UserDetailsService {

    @Autowired
    private EntityUsersRepository entityUsersRepository;
    @Override
    public CustomerUserDetails loadUserByUsername(String email) throws UsernameNotFoundException{
        EntityUsers user = entityUsersRepository.findByEmail(email);
        if(user == null){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "L'email ou le mot de passe est incorrect");
        }
        return new CustomerUserDetails(user.getEmail(), user.getPassword(), user.getId(), getGrantedAuthorities(user.getRole()));
    }

    private List<GrantedAuthority> getGrantedAuthorities(String role) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + role));
        return authorities;
    }
}
