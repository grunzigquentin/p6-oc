/// <reference types="Cypress" />
describe("It load the application", () => {
  beforeEach(() => {
    cy.visit("http://localhost:4200/");
  });
  it("should have a connection button that return to a login page", function () {
    cy.get(".login").click();
    cy.url().should("equal", "http://localhost:4200/login");
  });

  it("should have a connection button that return to a register  page", function () {
    cy.get(".register").click();
    cy.url().should("equal", "http://localhost:4200/register");
  });

  it("should have a header with a logo redirecting to home", function () {
    cy.get(".register").click();
    cy.url().should("equal", "http://localhost:4200/register");
    cy.get(".logoHeader").click();
    cy.url().should("equal", "http://localhost:4200/home");
  });

  it("The header only appear outside of the header page", function () {
    cy.get(".logoHeader").should("not.exist");
  });
});
