/// <reference types="Cypress" />
describe("It load the application", () => {
  beforeEach(() => {
    cy.visit("http://localhost:4200/register");
  });

  it("should have an arrow to go back to home page", function () {
    cy.get(".arrow").click();
    //cypress is not capable of using api of browser, thus cant use the location.back(). about:blank is the closest to reality
    cy.url().should("equal", "about:blank");
  });
});
