import { Component } from '@angular/core';
import {
  Router,
  RouterLink,
  RouterLinkActive,
  RouterOutlet,
} from '@angular/router';
import { HeaderComponent } from './shared/ui-components/header/header.component';
import { Location } from '@angular/common';
@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  styleUrl: './app.component.sass',
  imports: [RouterOutlet, RouterLink, RouterLinkActive, HeaderComponent],
})
export class AppComponent {
  title = 'frontend';
  currentUrl = 'home';
  constructor(private router: Router, private location: Location) {}

  ngOnInit() {
    this.router.events.subscribe((event) => {
      this.currentUrl = this.location.path();
    });
  }

  checkIfHomePage(): Boolean {
    if (this.currentUrl != '/home') return true;
    return false;
  }

  checkIfArrowAllowed(): Boolean {
    if (this.currentUrl == '/login' || this.currentUrl == '/register')
      return true;
    return false;
  }

  goBack(): void {
    this.location.back();
  }
}
