import { Routes } from '@angular/router';
import { LoginComponent } from './features/auth/login/login.component';
import { HomeComponent } from './features/home/home.component';
import { RegisterComponent } from './features/auth/register/register.component';
import { ArticleListComponent } from './features/articles/article-list/article-list.component';
import { AuthGuard, NotAuthGuard } from './features/auth/auth.guard';
import { ArticleComponent } from './features/articles/article/article.component';
import { CreateArticleComponent } from './features/articles/create-article/create-article.component';
import { ThemeListComponent } from './features/theme/theme-list/theme-list.component';
import { ProfileComponent } from './features/auth/profile/profile.component';

export const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [NotAuthGuard],
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [NotAuthGuard],
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [NotAuthGuard],
  },
  {
    path: 'articles',
    component: ArticleListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'article/:id',
    component: ArticleComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'new-article',
    component: CreateArticleComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'themes',
    component: ThemeListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard],
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'articles',
  },
];
