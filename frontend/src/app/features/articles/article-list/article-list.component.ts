import { Component, inject } from '@angular/core';
import { ButtonComponent } from '@shared/ui-components/button/button.component';
import { ArticlesService } from '../articles.service';
import { CommonModule } from '@angular/common';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Article } from '../models/article.model';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-article-list',
  standalone: true,
  imports: [ButtonComponent, CommonModule, RouterModule],
  templateUrl: './article-list.component.html',
  styleUrl: './article-list.component.sass',
})
export class ArticleListComponent {
  articleService = inject(ArticlesService);

  articles$ = this.articleService.getAll();
  sortedArticles: Article[] = [];
  ascendingSort: boolean | null = null;

  constructor() {
    this.articles$
      .pipe(takeUntilDestroyed())
      .subscribe((articles) => (this.sortedArticles = articles));
  }

  sortArticles() {
    this.sortedArticles.sort((a, b) => {
      if (this.ascendingSort)
        return (
          new Date(a.creationDate).getTime() -
          new Date(b.creationDate).getTime()
        );
      else
        return (
          new Date(b.creationDate).getTime() -
          new Date(a.creationDate).getTime()
        );
    });
    this.ascendingSort = !this.ascendingSort;
  }
}
