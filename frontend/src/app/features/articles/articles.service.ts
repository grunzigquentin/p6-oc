import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { Article, Comment } from './models/article.model';

@Injectable({
  providedIn: 'root',
})
export class ArticlesService {
  url = environment.apiURL + '/articles';

  http = inject(HttpClient);

  getAll(): Observable<Article[]> {
    return this.http.get<Article[]>(this.url);
  }
  getOne(id: string): Observable<Article> {
    return this.http.get<Article>(`${this.url}/${id}`);
  }

  createOne(article: Partial<Article>): Observable<Article> {
    return this.http.post<Article>(`${this.url}`, article);
  }

  comment(id: string, comment: { comment: string }): Observable<Comment> {
    return this.http.post<Comment>(`${this.url}/${id}/comment`, comment);
  }
}
