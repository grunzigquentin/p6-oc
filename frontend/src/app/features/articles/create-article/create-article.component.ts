import { CommonModule, Location } from '@angular/common';
import { Component, inject } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { ButtonComponent } from '@shared/ui-components/button/button.component';
import { InputComponent } from '@shared/ui-components/input/input.component';
import { TextAreaComponent } from '@shared/ui-components/text-area/text-area.component';
import { ThemeService } from 'app/features/theme/theme.service';
import { from } from 'rxjs';
import { ArticlesService } from '../articles.service';
import { Article } from '../models/article.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-article',
  standalone: true,
  imports: [
    NgSelectModule,
    ReactiveFormsModule,
    CommonModule,
    InputComponent,
    TextAreaComponent,
    ButtonComponent,
  ],
  templateUrl: './create-article.component.html',
  styleUrl: './create-article.component.sass',
})
export class CreateArticleComponent {
  fb = inject(FormBuilder);
  location = inject(Location);
  themeService = inject(ThemeService);
  articleService = inject(ArticlesService);
  router = inject(Router);
  form = this.fb.nonNullable.group({
    themes: ['', { validators: Validators.required }],
    title: ['', { validators: Validators.required }],
    content: ['', { validators: Validators.required }],
  });

  themeList$ = this.themeService.getThemeForUser();

  back() {
    this.location.back();
  }

  sendArticle() {
    if (this.form.valid) {
      this.articleService.createOne(this.form.value).subscribe({
        next: (article) => {
          this.router.navigate(['article/', article.id]);
        },
        error: () => console.error('error while creating article'),
      });
    }
  }
}
