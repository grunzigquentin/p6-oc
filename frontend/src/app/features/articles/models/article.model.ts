import { UserDto } from 'app/features/auth/models/user.model';

export interface Article {
  id: string;
  title: string;
  creationDate: Date;
  Author: string;
  content: string;
  theme: string;
  comments: Comment[];
}

export interface Comment {
  author: string;
  comment: string;
  entityUsers: UserDto;
}
