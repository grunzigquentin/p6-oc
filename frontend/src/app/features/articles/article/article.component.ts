import { Component, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  BehaviorSubject,
  Subject,
  combineLatest,
  map,
  startWith,
  switchMap,
  withLatestFrom,
} from 'rxjs';
import { ArticlesService } from '../articles.service';
import { CommonModule, Location } from '@angular/common';
import { TextAreaComponent } from '@shared/ui-components/text-area/text-area.component';
import { FormControl, Validators } from '@angular/forms';
import { Article, Comment } from '../models/article.model';

@Component({
  selector: 'app-article',
  standalone: true,
  imports: [CommonModule, TextAreaComponent],
  templateUrl: './article.component.html',
  styleUrl: './article.component.sass',
})
export class ArticleComponent {
  private route = inject(ActivatedRoute);
  private location = inject(Location);
  articleService = inject(ArticlesService);

  newComment$ = new BehaviorSubject<Comment | null>(null);
  article!: Article;

  commentControl = new FormControl('', { validators: Validators.required });

  constructor() {
    this.route.params
      .pipe(switchMap((param) => this.articleService.getOne(param['id'])))
      .subscribe((article) => (this.article = article));
  }

  sendComment(id: string) {
    if (this.commentControl.valid) {
      const comment = this.commentControl.value as string;
      this.articleService.comment(id, { comment }).subscribe({
        next: (response) => {
          this.article.comments = [...this.article.comments, response];
          this.commentControl.reset();
        },
        error: () => {
          console.error('problem when sending comment');
        },
      });
    }
  }

  back() {
    this.location.back();
  }
}
