import { Component, inject } from '@angular/core';
import { ThemeService } from '../theme.service';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ButtonComponent } from '@shared/ui-components/button/button.component';

@Component({
  selector: 'app-theme-list',
  standalone: true,
  imports: [CommonModule, RouterModule, ButtonComponent],
  templateUrl: './theme-list.component.html',
  styleUrl: './theme-list.component.sass',
})
export class ThemeListComponent {
  themeService = inject(ThemeService);

  themes$ = this.themeService.getAll();

  subscribe(id: string) {
    this.themeService.subscribeToTheme(id).subscribe();
  }
}
