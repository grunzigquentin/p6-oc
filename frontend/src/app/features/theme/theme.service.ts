import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { environment } from '@env/environment';
import { Theme } from './models/theme.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ThemeService {
  url = environment.apiURL + '/themes';
  http = inject(HttpClient);

  constructor() {}

  getThemeForUser() {
    return this.http.get<Theme[]>(`${this.url}/forUser`);
  }

  subscribeToTheme(id: string) {
    return this.http.post(`${this.url}/subscribe`, { themeId: id });
  }

  unsubscribeFromTheme(id: string): Observable<{ themeId: string }> {
    return this.http.post<{ themeId: string }>(`${this.url}/unsubscribe`, {
      themeId: id,
    });
  }

  getAll() {
    return this.http.get<Theme[]>(this.url);
  }
}
