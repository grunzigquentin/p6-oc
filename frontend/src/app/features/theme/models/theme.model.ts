export interface Theme {
  id: string;
  title: string;
  description: string;
}
