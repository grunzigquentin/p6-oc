import { Component, inject } from '@angular/core';
import { InputComponent } from '../../../shared/ui-components/input/input.component';
import { FormBuilder, Validators } from '@angular/forms';
import { StrongPasswordRegx } from '../../../shared/validators/password.regexp';
import { ButtonComponent } from '../../../shared/ui-components/button/button.component';
import { AuthService } from '../auth.service';
import { UserDto } from '../models/user.model';
import { Router } from '@angular/router';
import { NgIf } from '@angular/common';

@Component({
  selector: 'app-register',
  standalone: true,
  templateUrl: './register.component.html',
  styleUrl: './register.component.sass',
  imports: [InputComponent, ButtonComponent, NgIf],
})
export class RegisterComponent {
  fb = inject(FormBuilder);
  authService = inject(AuthService);
  router = inject(Router);

  errorMessage = null;

  form = this.fb.group({
    username: ['', { validators: [Validators.required] }],
    email: ['', { validators: [Validators.required, Validators.email] }],
    password: [
      '',
      {
        validators: [
          Validators.required,
          Validators.pattern(StrongPasswordRegx),
        ],
      },
    ],
  });

  submit() {
    if (this.form.valid) {
      this.authService.register(this.form.value as UserDto).subscribe({
        next: ({ token }) => {
          localStorage.setItem('jwt', token);
          this.authService.logInStatusChange.next(true);
          this.router.navigate(['/articles']);
        },
        error: ({ error }) => {
          this.errorMessage = error.message;
        },
      });
    }
  }
}
