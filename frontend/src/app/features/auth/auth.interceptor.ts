import { HttpRequest, HttpEvent, HttpHandlerFn } from '@angular/common/http';
import { inject } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { Router } from '@angular/router';

export function authInterceptor(
  req: HttpRequest<unknown>,
  next: HttpHandlerFn
): Observable<HttpEvent<unknown>> {
  const router = inject(Router);
  const jwToken = localStorage.getItem('jwt');
  const newRequest = jwToken
    ? req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + jwToken),
      })
    : req;

  return next(newRequest).pipe(
    tap({
      error: (error: any) => {
        if (error.status === 401) {
          localStorage.removeItem('jwt');
          router.navigate(['/home'], { replaceUrl: true });
        }
      },
    })
  );
}
