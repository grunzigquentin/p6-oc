import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { ProfileDTO, UserDto, logInDto } from './models/user.model';
import { Observable, Subject } from 'rxjs';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  url = environment.apiURL + '/users';
  http = inject(HttpClient);

  logInStatusChange = new Subject<boolean>();

  register(user: UserDto): Observable<{ token: string }> {
    return this.http.post<{ token: string }>(`${this.url}/register`, user);
  }

  logIn(user: logInDto): Observable<{ token: string }> {
    return this.http.post<{ token: string }>(`${this.url}/login`, user);
  }

  updateProfile(user: ProfileDTO): Observable<{ token: string }> {
    return this.http.put<{ token: string }>(`${this.url}/profile`, user);
  }

  static isLoggedIn(): boolean {
    const jwt = localStorage.getItem('jwt');
    return jwt !== null;
  }
}
