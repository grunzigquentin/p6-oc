import { Component, inject } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { logInDto } from '../models/user.model';
import { InputComponent } from '../../../shared/ui-components/input/input.component';
import { ButtonComponent } from '../../../shared/ui-components/button/button.component';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [InputComponent, ButtonComponent, ReactiveFormsModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.sass',
})
export class LoginComponent {
  fb = inject(FormBuilder);
  authService = inject(AuthService);
  router = inject(Router);

  errorMessage = null;

  form = this.fb.group({
    email: ['', { validators: [Validators.required] }],
    password: ['', { validators: [Validators.required] }],
  });

  submit() {
    if (this.form.valid) {
      this.authService.logIn(this.form.value as logInDto).subscribe({
        next: ({ token }) => {
          localStorage.setItem('jwt', token);
          this.authService.logInStatusChange.next(true);
          this.router.navigate(['/articles']);
        },
        error: ({ message }) => {
          this.errorMessage = message;
        },
      });
    }
  }
}
