import { inject } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

export const AuthGuard = () => {
  const router = inject(Router);

  if (!AuthService.isLoggedIn()) {
    router.navigateByUrl('/home');
    return false;
  }
  return true;
};

export const NotAuthGuard = () => {
  const router = inject(Router);

  if (AuthService.isLoggedIn()) {
    router.navigateByUrl('/articles');
    return false;
  }
  return true;
};
