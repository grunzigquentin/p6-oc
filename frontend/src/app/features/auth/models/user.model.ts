export interface UserDto {
  username: string;
  email: string;
  password: string;
}

export interface logInDto {
  email: string;
  password: string;
}

export interface ProfileDTO {
  userName: string;
  email: string;
}
