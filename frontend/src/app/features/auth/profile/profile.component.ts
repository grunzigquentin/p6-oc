import { Component, inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ButtonComponent } from '@shared/ui-components/button/button.component';
import { InputComponent } from '@shared/ui-components/input/input.component';
import { AuthService } from '../auth.service';
import { ProfileDTO } from '../models/user.model';
import { Router } from '@angular/router';
import { ThemeService } from 'app/features/theme/theme.service';
import { CommonModule } from '@angular/common';
import { BehaviorSubject, combineLatest, map } from 'rxjs';
import { Theme } from 'app/features/theme/models/theme.model';

@Component({
  selector: 'app-profile',
  standalone: true,
  imports: [InputComponent, ButtonComponent, CommonModule],
  templateUrl: './profile.component.html',
  styleUrl: './profile.component.sass',
})
export class ProfileComponent {
  fb = inject(FormBuilder);
  authService = inject(AuthService);
  themeService = inject(ThemeService);
  router = inject(Router);

  removedThemeId$ = new BehaviorSubject<string>('');
  themes: Theme[] = [];

  constructor() {
    this.themeService.getThemeForUser().subscribe((themes) => {
      this.themes = themes;
    });
  }

  form = this.fb.nonNullable.group({
    username: ['', { validators: [Validators.required] }],
    email: ['', { validators: [Validators.required, Validators.email] }],
  });

  saveProfile() {
    if (this.form.valid) {
      this.authService.updateProfile(this.form.value as ProfileDTO).subscribe();
    }
  }

  logout() {
    localStorage.removeItem('jwt');
    this.router.navigate(['/home']);
  }

  unsubscribe(id: string) {
    this.themeService.unsubscribeFromTheme(id).subscribe({
      next: (response) =>
        (this.themes = this.themes.filter((t) => t.id !== response.themeId)),
      error: () => console.error('Error while unsubscribing'),
    });
  }
}
