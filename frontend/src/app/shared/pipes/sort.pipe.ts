import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort',
  standalone: true,
})
export class SortPipe implements PipeTransform {
  transform(array: any[], field: string) {
    if (!Array.isArray(array)) {
      return;
    }
    array.sort((a, b) => this.dateSort(a[field], b[field]));
    return array;
  }

  dateSort(a: Date, b: Date) {
    return a < b ? -1 : a > b ? 1 : 0;
  }
}
