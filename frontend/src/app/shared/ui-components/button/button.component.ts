import { Component, input, output } from '@angular/core';

@Component({
  selector: 'app-button',
  standalone: true,
  imports: [],
  templateUrl: './button.component.html',
  styleUrl: './button.component.sass',
})
export class ButtonComponent {
  isSecondary = input<boolean>(false);
  clicked = output();
  disabled = input<boolean>(false);

  onClick() {
    this.clicked.emit();
  }
}
