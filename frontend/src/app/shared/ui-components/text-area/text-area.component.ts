import { Component, input } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-text-area',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './text-area.component.html',
  styleUrl: './text-area.component.sass',
})
export class TextAreaComponent {
  control = input.required<FormControl>();
  placeHolder = input<string>('');
  inputName = input.required<string>();
  label = input<string>('');
}
