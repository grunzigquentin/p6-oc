import { NgIf } from '@angular/common';
import { Component, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { RouterLink } from '@angular/router';
import { AuthService } from 'app/features/auth/auth.service';
import { LoginComponent } from 'app/features/auth/login/login.component';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [RouterLink, NgIf],
  templateUrl: './header.component.html',
  styleUrl: './header.component.sass',
})
export class HeaderComponent {
  authService = inject(AuthService);
  isLoggedIn = AuthService.isLoggedIn();

  constructor() {
    this.authService.logInStatusChange
      .pipe(takeUntilDestroyed())
      .subscribe((logInStatus) => {
        this.isLoggedIn = logInStatus;
      });
  }

  showAside = false;
  toggleAside() {
    this.showAside = !this.showAside;
  }

  closeAside(event: MouseEvent) {
    this.showAside = false;
  }
}
