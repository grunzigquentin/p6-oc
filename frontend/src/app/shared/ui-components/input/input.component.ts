import { Component, input } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';

type InputTypes =
  | 'text'
  | 'email'
  | 'password'
  | 'number'
  | 'search'
  | 'url'
  | 'date'
  | 'datetime-local'
  | 'time';

@Component({
  selector: 'app-input',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './input.component.html',
  styleUrl: './input.component.sass',
})
export class InputComponent {
  control = input.required<FormControl>();
  placeHolder = input<string>('');
  type = input<InputTypes>('text');
  inputName = input.required<string>();
  label = input<string>('');
}
